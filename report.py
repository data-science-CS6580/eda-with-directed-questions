from multiprocessing.sharedctypes import Value
import requests
from bs4 import BeautifulSoup
import matplotlib.pyplot as plt
from scipy import stats
import pandas as pd
import numpy as np
import csv
import signal
import sys

SCRAPPING_ON = True #due to how slow scraping is, this will bypass it for testing

marriedMalePassengersWithWifeOnBoard = []
marriedPassengers = []
singlePassengers = []
totalPassengers = 0

marriedCrew = []
marriedMaleCrewWithWifeOnBoard = []
singleCrew = []
totalCrew = 0

def killit():
    print("exiting")
    sys.exit(0)


def scrapeMaritalStatus(link, isMale,passenger=True):
    onesSelf = None
    try:
        response = requests.get(link, headers={'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:84.0) Gecko/20100101 Firefox/84.0'})
        soup = BeautifulSoup(response.text, 'html.parser')
        onesSelf = soup.find('h1', class_ = None)

        spouse = soup.find(attrs={"itemprop": "spouse"})
        if spouse != None: #if spouse is not empty
            if isMale: # if male check to see if wife is on board 
                smallTags = soup.find_all('small')
                for tag in smallTags:
                    if tag.text == "Wife": #check all small tags to see if any of them say "wife" if yes add to list
                        if passenger:
                            marriedMalePassengersWithWifeOnBoard.append(onesSelf.text)
                            marriedPassengers.append(onesSelf.text)
                        else:
                            marriedMaleCrewWithWifeOnBoard.append(onesSelf.text)
                            marriedCrew.append(onesSelf.text)
                        return
                
            if passenger: #if female or male's wife is not on board
                marriedPassengers.append(onesSelf.text)
            else:
                marriedCrew.append(onesSelf.text)
        else: #else if you are single
            if passenger:
                singlePassengers.append(onesSelf.text)
            else:
                singleCrew.append(onesSelf.text)
            
    except KeyboardInterrupt:
        sys.exit(0)
    except:
        #it probably got an error which means its single
        try:
            if passenger:
                singlePassengers.append(onesSelf.text)
            else:
                singleCrew.append(onesSelf.text)
        except:
            pass


signal.signal(signal.SIGINT, killit)

passengersWithBurialInfo = 0
passengersWithOutBurialInfo = 0
passengersBurialInfo = []
passengerNationalitys = []
passengerAges = []
with open('./files/Titanic_Passenger_List.csv') as csvFile:
    csvReader = csv.reader(csvFile, delimiter=',')
    line_count = 0
    
    for row in csvReader:
        if line_count == 0:
            line_count += 1
        else:
            line_count += 1
            passengerNationalitys.append(row[14])
            try:
                passengerAges.append(int(row[3]))
            except ValueError:
                try:
                    s = row[3]
                    s = s.replace("m", "")
                    age = int(s)/12
                    passengerAges.append(age)
                except ValueError:
                    pass
            if SCRAPPING_ON:
                if row[4] == "Female":
                    scrapeMaritalStatus(row[15],False)
                else:
                    scrapeMaritalStatus(row[15], True)
            if row[11] == "LOST":
                if row[13] != None and row[13] != "":
                    passengersWithBurialInfo += 1
                    passengersBurialInfo.append(row[11])
                else:
                    passengersWithOutBurialInfo += 1

    
    line_count -= 1
    totalPassengers = line_count
    print(f'Number of passengers aboard: {line_count}')
    #print(passengerNationalitys)

#Name,Born,Died,Age,Gender,Class/Dept,Ticket,Fare,Cabin,Joined,Occupation,Survived?,Boat,Body,Nationality,URL 15
cewWithBurialInfo = 0
cewWithOutBurialInfo = 0
cewBurialInfo = []
crewNationalities = []
crewAges = []
crewAgesSurvived = []
crewAgesDied = []
with open('./files/Titanic_Crew.csv') as csvFile:
    csvReader = csv.reader(csvFile, delimiter=',')
    line_count = 0

    for row in csvReader:
        if line_count == 0:
            line_count += 1
        else:
            try:
                crewAges.append(int(row[3]))
                if row[11] == "LOST":
                    crewAgesDied.append(int(row[3]))
                else:
                    crewAgesSurvived.append(int(row[3]))
            except ValueError:
                try:
                    s = row[3]
                    s = s.replace("m", "")
                    age = int(s)/12
                    crewAges.append(age)
                    if row[11] == "LOST":
                        crewAgesDied.append(age)
                    else:
                        crewAgesSurvived.append(age)
                except:
                    pass
            line_count += 1
            crewNationalities.append(row[14])
            if SCRAPPING_ON:
                if row[4] == "Female":
                    scrapeMaritalStatus(row[15],False, False)
                else:
                    scrapeMaritalStatus(row[15], True, False)
            if row[11] == "LOST":
                if row[13] != None and row[13] != "":
                    cewWithBurialInfo += 1
                    cewBurialInfo.append(row[11])
                else:
                    cewWithOutBurialInfo += 1
    
    line_count -= 1
    totalCrew = line_count
    print(f'Number of crew aboard: {line_count}')

print("People who were in both cew and passengers: 0")
print("Number of single passengers: ", len(singlePassengers))
print("Number of married passengers: ", len(marriedPassengers))
print("Number of married passenger males with wife on board: ", len(marriedMalePassengersWithWifeOnBoard))
print("")
print("Number of single crew: ", len(singleCrew))
print("Number of married crew: ", len(marriedCrew))
print("Number of married males whoes wife on board: ", len(marriedMaleCrewWithWifeOnBoard))


####GROUP BAR CHART MARITALSTATUS######
passengers = []
crew = []

if SCRAPPING_ON:
    passengers = [1, len(singlePassengers), len(marriedPassengers), len(marriedMalePassengersWithWifeOnBoard)]
    crew = [0, len(singleCrew), len(marriedCrew), len(marriedMaleCrewWithWifeOnBoard)]
else:
    passengers = [1, 905, 447, 54]
    crew = [0, 660, 465, 0]


gbcLabels = ['Divorced', 'Single', 'Married', 'Men w/spouse']
x = np.arange(len(gbcLabels))
gbcfig, gbc = plt.subplots()
rects1 = gbc.bar(x - 0.35/2, passengers, 0.35, label="Passengers")
rects2 = gbc.bar(x + 0.35/2, crew, 0.35, label="Crew")
gbc.set_ylabel("Number of people")
gbc.set_title("Number of People by Pass/Crew and Marital Status")
gbc.set_xticks(x, gbcLabels )
gbc.legend()
gbc.bar_label(rects1, padding=3)
gbc.bar_label(rects2, padding=3)
gbcfig.tight_layout()
plt.savefig('./reports/groupBarChartMaritalStatus')

###marital percent
maritalPieSizes = [passengers[0]/totalPassengers, passengers[1]/totalPassengers, passengers[2]/totalPassengers, (passengers[3]*2)/totalPassengers]
explode = (0,0,0,0.1) #explode last slice "Men w/spouse"

mpiefig, mPie = plt.subplots()
gbcLabels[3] = "Had spouse"
mPie.pie(maritalPieSizes, explode=explode, labels=gbcLabels, shadow=True, autopct='%1.1f%%')
mPie.axis('equal')
mPie.set(title="Marital Status by Percentage")
plt.savefig('./reports/percentageOfMenHadSpouse')

#####Burial Info:
print("\nPassengers with burial info: ",passengersWithBurialInfo/(passengersWithBurialInfo+passengersWithOutBurialInfo)*100, "%")
print("Crew with burial info: ", cewWithBurialInfo/(cewWithBurialInfo+cewWithOutBurialInfo)*100, "%")
print(cewWithBurialInfo,cewWithBurialInfo,cewWithOutBurialInfo)
print(passengersWithBurialInfo,passengersWithBurialInfo,passengersWithOutBurialInfo)


###Nationalities
nationalitiesDict ={}
for nat in crewNationalities:
    if nat in nationalitiesDict.keys():
        nationalitiesDict[nat] += 1
    else:
        nationalitiesDict[nat] = 1
natLabels = []
natValues = []
for key in nationalitiesDict:
    natLabels.append(key)
    natValues.append(nationalitiesDict[key])

plt.rcdefaults()
nationalitiesFig, natBar = plt.subplots()
y_pos = np.arange(len(natLabels))
natBar.barh(y_pos, natValues, align='center')
natBar.set_yticks(y_pos, labels=natLabels)
natBar.invert_yaxis()
natBar.set_xlabel("Number of People")
natBar.set_title("Crew Nationalities")
plt.savefig('./reports/nationalitiesBarChart')
print("\n")
#thanks to the textbook:
df = pd.read_csv("./files/Titanic_Crew.csv")
contingency_table = pd.crosstab(df['Nationality'], df['Survived?'])
print(contingency_table)
print("\n", stats.chi2_contingency(pd.crosstab(df["Nationality"],df["Survived?"]))[0:3])


###Ages
print("\nPassenger Ages: ")
print("Youngest: Gilbert Sigvard Emanuel 4m")
print("Oldest: SVENSSON, Mr Johan 74")
passengerAges = pd.Series(passengerAges)
print(passengerAges.describe())
print("\nCrew Ages:")
print("Youngest: HOPKINS, Mr Frederick William and WATSON, Mr William Albert 14")
print("Oldest: O'LOUGHLIN, Dr William Francis Norman 63")
crewAges = pd.Series(crewAges)
print(crewAges.describe())

ageBoxFig, ageBox = plt.subplots()
ageBox.violinplot([crewAges, crewAgesDied, crewAgesSurvived], positions=[2,4,6], widths=1.5, showmeans=True)
ageBox.set_xticks([2,4,6], ["All Crew", "Died", "Survived" ])
plt.savefig("./reports/agesViolinPlot")

contingency_table = pd.crosstab(df['Gender'], df['Survived?'])
print("\nDid more women survive than men percentage-wise for the crew?")
print(contingency_table)
print("\n", stats.chi2_contingency(pd.crosstab(df["Gender"],df["Survived?"]))[0:3])

exit(0)
