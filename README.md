# Titanic Exploratory Data Analysis
## With directed questions

Information comes from [Encyclopedia Titanica](https://www.encyclopedia-titanica.org/)

#### Running the Code
You will need the following already installed using pip3:
- BeautifulSoup
- matplotlib   

To run:
```sh
python3 report.py
```

#### Viewing Table
If you just want to view the reports visit [/reports](https://gitlab.com/data-science-CS6580/eda-with-directed-questions/-/tree/main/reports)    
Read the [EDA](https://gitlab.com/data-science-CS6580/eda-with-directed-questions/-/blob/main/TitanicEDA.pdf)